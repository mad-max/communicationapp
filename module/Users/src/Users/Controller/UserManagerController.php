<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Users\Model\User;
use Users\Model\UserTable;

class UserManagerController extends AbstractActionController {
    
    
    public function indexAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $viewModel = new ViewModel(array('users' => $userTable->fetchAll()));
        return $viewModel;
    }
    
    public function editAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user = $userTable->getUser($this->params()->fromRoute('id'));
        $form = $this->getServiceLocator()->get('UserEditForm');
        $form->bind($user);
        $viewModel = new ViewModel(array(
            'form' => $form,
            'user_id' => $this->params()->fromRoute('id')
        ));
        return $viewModel;
    }   //editAction
    
    public function processAction() {
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute('users/user-manager', array('action' => 'edit')); //Ошибка! Надо передавать Id
        }
        $post = $this->request->getPost();   //получение id пользователя из POST
        $userTable = $this->getServiceLocator()->get('UserTable'); 
        $user = $userTable->getUser($post->id);  //загрузка сущности User
        
        $form = $this->getServiceLocator()->get('UserEditForm'); 
        /*
         * Привязка сущности User к Form
         * форма заполняется данными из $user
         * гидратор передает проверенные данные для гидратации (заполнения) 
         * привязанного объекта (т.е. проверенные данные передаются в $user
         */
        $form->bind($user);  
        /*
         * форма заполняется данными из $post
         */
        $form->setData($post);  
        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error' => true,
                'form' => $form,
            ));
            $model->setTemplate('users/user-manager/edit'); //Здесь надо вручную устанавливать шаблон, т.к. в папке представлений нет шаблона(представления) process. А zf2 ищет сначала папку с названием контроллера, затем файл шаблона с названием действия(action)
            return $model;
        }
        $this->getServiceLocator()->get('UserTable')->saveUser($user);   //сохранение пользователя
        return $this->redirect()->toRoute('users/user-manager');
    }
    
    public function deleteAction() {
        $this->getServiceLocator()->get('UserTable')
                ->deleteUser($this->params()->fromRoute('id'));
        return $this->redirect()->toRoute('users/user-manager');
    }
    
}


















