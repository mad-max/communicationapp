<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class IndexController extends \Zend\Mvc\Controller\AbstractActionController {
    
    /*
     * Если пользователь посещает домашнюю страницу, то ему выводится 
     * представление, предлагаемое по умолчанию.
     */
    public function indexAction() {
        $view=new ViewModel();
        return $view;
    }
    
    /*
     * Если пользователь совершает действие register, то ему выводится 
     * шаблон newuser.
     */
    public function registerAction() {
        $view=new ViewModel();
        $view->setTemplate('users/index/new-user');
        return $view;
    }
    
    /*
     * Если пользователь совершает действие login, то выводится шаблон login.
     */
    public function loginAction() {
        $view=new ViewModel();
        $view->setTemplate('users/index/login');
        return $view;
    }
    
    
}   //IndexController



























