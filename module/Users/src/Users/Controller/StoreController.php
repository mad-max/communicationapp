<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class StoreController extends AbstractActionController {
    
    public function indexAction() {
        $productTable = $this->getServiceLocator()->get('StoreProductsTable');
        $storeProducts = $productTable->fetchAll();
        
        $viewModel = new ViewModel(
            array(
                'storeProducts' => $storeProducts
            )
        );
        
        return $viewModel;
    }
    
    
    public function productDetailAction() {
        $productId = $this->params()->fromRoute('id');
        $productTable = $this->getServiceLocator()->get('StoreProductsTable');
        $product = $productTable->getProduct($productId);
        
        
        $form = new \Zend\Form\Form();
        
        $form->add(array(
            'name' => 'qty',
            'attributes' => array(
                'type' => 'text',
                'id' => 'qty',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Quantity',
            ),
        ));
        
        $form->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Purchase'
            ),
        ));
        
        $form->add(array(
            'name' => 'store_product_id',
            'attributes' => array(
                'type' => 'hidden',
                'value' => $product->id
            ),
        ));
        
        $viewModel = new ViewModel(array(
            'product' => $product,
            'form' => $form
        ));
        
        return $viewModel;
        
    }
    
    
    public function shoppingCartAction() {
        
        $request = $this->getRequest();
        
        $productId = $request->getPost()->get('store_product_id');
        $quantity = $request->getPost()->get('qty');
        
        $orderTable = $this->getServiceLocator()->get('StoreOrdersTable');
        $productTable = $this->getServiceLocator()->get('StoreProductsTable');
        $product = $productTable->getProduct($productId);
        
        
        $newOrder = new \Users\Model\StoreOrder($product);
        $newOrder->setQuantity($quantity);
        
        $orderId = $orderTable->saveOrder($newOrder);
        
        $order = $orderTable->getOrder($orderId);
        $viewModel = new ViewModel(array(
            'order' => $order,
//            'productId' => $order->getProduct()->id,
//            'productName' => $order->getProduct()->name,
//            'productQty' => $order->qty,
//            'unitCost' => $order->getProduct()->cost,
//            'total' => $order->total,
//            'orderId' => $order->id,
        ));
        
        return $viewModel;        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}
