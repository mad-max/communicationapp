<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Http\Headers;

use Users\Model\User;
use Users\Model\UserTable;
use Users\Model\Upload;

use Users\Model\ImageUpload;
use Users\Model\ImageUploadTable;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use Users\Form\RegisterForm;
use Users\Form\RegisterFilter;
use Users\Form\ImageUploadForm;
use Users\Form\ImageUploadGoogleFormForm;

use Zend\Config\Reader\Ini;


class MediaManagerController extends AbstractActionController {
    
    protected $storage;
    protected $authservice;
    
    protected $photos;
    
    protected $GOOGLE_USER_ID;
    protected $GOOGLE_PASSWORD;
    
                
    /*
     * Функция возвращает службу аутентификации
     */
    public function getAuthService() {
        if (!$this->authservice) {
           $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
        
        return $this->authservice;
        
    }    //getAuthService
    
    
    /*
     * Получение конфигурации из конфигурационных данных модуля
     * 
     * Iterator — это интерфейс SPL который предназначен для реализации 
     * объектов с циклической структурой. Реализация данного интерфейса в 
     * разрабатываемых классах позволяет стандартным функциям PHP 
     * (например foreach) работать с пользовательскими объектами.
     * 
     * Интерфейс Traversable - интерфейс, определяющий, является ли класс 
     * обходимым (traversable) используя foreach. 
     * 
     * iteratorToArray() - Convert an iterator to an array.
     */
    public function getFileUploadLocation() {
        $config  = $this->getServiceLocator()->get('config');
        if ($config instanceof \Traversable) { 
            $config = ArrayUtils::iteratorToArray($config);
        }
        if (!empty($config['module_config']['image_upload_location'])) {
            return $config['module_config']['image_upload_location'];
        } else {
            return FALSE;
        }
    }
    
    /*
     * Возвращает страницу со списком выгруженных файлов юзера
     */
    public function indexAction() {
        
        $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        //Получение информации о пользователе от сеанса
        $userEmail = $this->getAuthService()->getStorage()->read(); //в LoginController в хранилище записывается только email
        $user = $userTable->getUserByEmail($userEmail);
        $googleAlbums = $this->getGooglePhotos();
        $youtubeVideos = $this->getYoutubeVideos();
        
        $viewModel  = new ViewModel(
            array(
                'myUploads' => $uploadTable->getUploadsByUserId($user->id),
                'googleAlbums' => $googleAlbums,
                'youtubeVideos' => $youtubeVideos,
                )
        );
        
        return $viewModel;
        
    }
    
    
    /*
     * Загружаем файл на сервер
     */
    public function processUploadAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');   	
        $user_email = $this->getAuthService()->getStorage()->read();    	 
        $user = $userTable->getUserByEmail($user_email);
        $form = $this->getServiceLocator()->get('ImageUploadForm');
        $request = $this->getRequest();

        if ($request->isPost()) {
            $upload = new ImageUpload();
            $uploadFile = $this->params()->fromFiles('imageupload');    //получает данные о файле из формы поле imageupload
            
            $form->setData($request->getPost());
            
            
            if ($form->isValid()) {
                //Получение конфигурации из конфигурационных данных модуля
                $uploadPath = $this->getFileUploadLocation();
                
                //Сохранение выгруженного файла
                $adapter = new \Zend\File\Transfer\Adapter\Http();
                $adapter->setDestination($uploadPath);
                if ($adapter->receive($uploadFile['name'])) {
                   
                    //Успешная выгрузка файла
                    $exchange_data = array();
                    $exchange_data['label'] = $request->getPost()->get('label');
                    $exchange_data['filename'] = $uploadFile['name'];
                    $exchange_data['thumbnail'] = $this->generateThumbnail($uploadFile['name']);
                    $exchange_data['user_id'] = $user->id;

                    $upload->exchangeArray($exchange_data);
                    $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
                    $uploadTable->saveUpload($upload);
                    
                    return $this->redirect()->toRoute('users/media', 
                            array('action' => 'index'));
                }
            }
        }
        return array('form' => $form);
        
    }   //processUploadAction
    
    
    public function generateThumbnail($imageFileName) {
        $path = $this->getFileUploadLocation();
        $sourceImageFileName = $path . '/' . $imageFileName;
        $thumbnailFileName = 'th_' . $imageFileName;
        
        $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
        $thumb = $imageThumb->create($sourceImageFileName, $options = array());
        $thumb->resize(75, 75);
        $thumb->save($path . '/' . $thumbnailFileName);
        
        return $thumbnailFileName;
    }
    
    /*
     * Возвращает страницу для загрузки на сервер файла 
     */
    public function uploadAction() {
        $form = $this->getServiceLocator()->get('ImageUploadForm');
        $viewModel  = new ViewModel(array('form' => $form)); 
        return $viewModel; 
    }
    
    /*
     * Возвращает страницу для загрузки файла в альбомы Google photos
     */
    public function uploadGoogleAction() {
        
        $form = $this->getServiceLocator()->get('ImageUploadGoogleForm');
        
        $reader = new Ini();
        $dataAuthGoogle = $reader->fromFile(__DIR__ . '/../../../config/google.ini');
        
        $this->GOOGLE_USER_ID = $dataAuthGoogle['username'];
	$this->GOOGLE_PASSWORD = $dataAuthGoogle['password'];
        
        
        /*
         * отключаем sslverifypeer
         */
        $adapter = new \Zend\Http\Client\Adapter\Curl();
        $adapter->setOptions(array(
            'curloptions' => array(
                CURLOPT_SSL_VERIFYPEER => false,
            )
        ));
        
        $httpClient = new \ZendGData\HttpClient();
        $httpClient->setAdapter($adapter);
        
        $client = \ZendGData\ClientLogin::getHttpClient(
                $this->GOOGLE_USER_ID,
                $this->GOOGLE_PASSWORD,
                \ZendGData\Photos::AUTH_SERVICE_NAME,
                $httpClient);
        
        /*
         * создать нового клиента Google Photos с помощью API-клиента
         */
        $gp = new \ZendGData\Photos($client);
        
        $gAlbums = array();
        
        try {
            $userFeed = $gp->getUserFeed($this->GOOGLE_USER_ID);    //Получить список альбомов с помощью функции getUserFeed()
            foreach ($userFeed as $userEntry) {
                $albumId = $userEntry->getGphotoId()->getText();    //id альбома
                $gAlbums[$albumId] = $userEntry->getTitle()->getText();    //массив id альбома=>название альбома
            
                $form->get('album_id')->setValueOptions($gAlbums);  //получить из формы элемент список и установить в него массив с альбомами
            }
        } catch (App\HttpException $ex) {
            echo "Error:" . $ex->getMessage() . "<br />\n";
            if ($ex->getResponse() != null) {
                echo "Body: <br />\n" . $ex->getResponse()->getBody() . "<br />\n";
            }
        }
                
        $viewModel = new ViewModel(array('form' => $form));   //создать модель
            
        return $viewModel; 
        
    }
    
    
    /*
     * Удалить загруженный файл и запись о нем 
     */
    public function deleteAction() {
        
        $uploadId = $this->params()->fromRoute('id');   //получить id файла из get запроса
        $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');  //ссылка на таблицу 
        $upload = $uploadTable->getUpload($uploadId);   //получить из бд данные о файле
        $uploadPath = $this->getFileUploadLocation();
        unlink($uploadPath . "/" . $upload->filename);  //удалить файл
        unlink($uploadPath . "/" . $upload->thumbnail);  //удалить эскиз
        $uploadTable->deleteUpload($uploadId);  //удалить запись в бд
        
        return $this->redirect()->toRoute('users/media');
    }   //deleteAction
    
    
    /*
     * Действие, которое выводит изображение в режимах Full
     * (в натуральную величину) и Thumbnail(эскиз)
     */
    public function viewAction() {
        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $upload = $uploadTable->getUpload($uploadId);
        
        $viewModel = new ViewModel(array('upload' => $upload));
        return $viewModel;
    }
    
    /*
     * Поворачивает изображение влево и сохраняет
     */
    public function RotateLeftAction() {
        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $upload = $uploadTable->getUpload($uploadId);
        
        
        $path = $this->getFileUploadLocation();
        $sourceImageFileName = $path . '/' . $upload->filename;
        $thumbnailFilename = $path . '/' . $upload->thumbnail;
        
        $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
        
        $thumb = $imageThumb->create($sourceImageFileName, $options = array());
        $thumb->rotateImage($direction = 'CW');
        $thumb->save($path . '/' . $upload->filename);
        
        $thumb = $imageThumb->create($thumbnailFilename, $options = array());
        $thumb->rotateImage($direction = 'CW');
        $thumb->save($path . '/' . $upload->thumbnail);
        
        return $this->redirect()->toRoute('users/media', array('action' => 'view', 'id' => $uploadId));
        
//        $viewModel = new ViewModel(array('upload' => $upload));
//        $viewModel->setTemplate('users/media-manager/view');
//        return $viewModel;
    }
    
    /*
     * Поворачивает изображение вправо и сохраняет
     */
    public function RotateRightAction() {
        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $upload = $uploadTable->getUpload($uploadId);
        
        
        $path = $this->getFileUploadLocation();
        $sourceImageFileName = $path . '/' . $upload->filename;
        $thumbnailFilename = $path . '/' . $upload->thumbnail;
        
        $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
        
        $thumb = $imageThumb->create($sourceImageFileName, $options = array());
        $thumb->rotateImage($direction = 'СCW');
        $thumb->save($path . '/' . $upload->filename);
        
        $thumb = $imageThumb->create($thumbnailFilename, $options = array());
        $thumb->rotateImage($direction = 'СCW');
        $thumb->save($path . '/' . $upload->thumbnail);
        
        return $this->redirect()->toRoute('users/media', array('action' => 'view', 'id' => $uploadId));
        
//        $viewModel = new ViewModel(array('upload' => $upload));
//        $viewModel->setTemplate('users/media-manager/view');
//        return $viewModel;
    }
    
    
    public function showImageAction() {
               
        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $upload = $uploadTable->getUpload($uploadId);
        
        $uploadPath = $this->getFileUploadLocation();
        
        if ($this->params()->fromRoute('subaction') == 'thumb') {
            $filename = $uploadPath . "/" . $upload->thumbnail;
        } else {
            $filename = $uploadPath . "/" . $upload->filename;
        }
        
        $file = file_get_contents($filename);   //Получить содержимое файла в виде одной строки.
        
        //прямой возврат ответа
        $response = $this->getEvent()->getResponse();
        $response->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment;filename="' . $upload->filename . '"',
        ));
        $response->setContent($file);
        return $response;
    }
    
    
    public function getGooglePhotos() {
        
        $reader = new Ini();
        $dataAuthGoogle = $reader->fromFile(__DIR__ . '/../../../config/google.ini');
        
        $this->GOOGLE_USER_ID = $dataAuthGoogle['username'];
	$this->GOOGLE_PASSWORD = $dataAuthGoogle['password'];
        
        
        /*
         * отключаем sslverifypeer
         */
        $adapter = new \Zend\Http\Client\Adapter\Curl();
        $adapter->setOptions(array(
            'curloptions' => array(
                CURLOPT_SSL_VERIFYPEER => false,
            )
        ));
        
        $httpClient = new \ZendGData\HttpClient();
        $httpClient->setAdapter($adapter);
        
        $client = \ZendGData\ClientLogin::getHttpClient(
                $this->GOOGLE_USER_ID,
                $this->GOOGLE_PASSWORD,
                \ZendGData\Photos::AUTH_SERVICE_NAME,
                $httpClient);
        
        /*
         * создать нового клиента Google Photos с помощью API-клиента
         */
        $gp = new \ZendGData\Photos($client);
        
        $gAlbums = array();
        
        try {
            $userFeed = $gp->getUserFeed($this->GOOGLE_USER_ID);    //Получить список альбомов с помощью функции getUserFeed()
            foreach ($userFeed as $userEntry) {
                $albumId = $userEntry->getGphotoId()->getText();    //id альбома
                $gAlbums[$albumId]['label'] = $userEntry->getTitle()->getText();    //название альбома
                
                $query = $gp->newAlbumQuery();
                $query->setUser($this->GOOGLE_USER_ID);
                $query->setAlbumId($albumId);
                
                $albumFeed = $gp->getAlbumFeed($query); //Получить список изображений в альбоме с помощью функции getAlbumFeed()
                
                foreach ($albumFeed as $photoEntry) {
                    $photoId = $photoEntry->getGphotoId()->getText();
                    if ($photoEntry->getMediaGroup()->getContent() != null) {
                        $mediaContentArray = $photoEntry->getMediaGroup()->getContent();
                        $photoUrl = $mediaContentArray[0]->getUrl();
                    }
                    
                    if ($photoEntry->getMediaGroup()->getThumbnail() != null) {
                        $mediaThumbnailArray = $photoEntry->getMediaGroup()->getThumbnail();
                        $thumbUrl = $mediaThumbnailArray[0]->getUrl();
                    }
                   
                    $albumPhoto = array();
                    $albumPhoto['id'] = $photoId;
                    $albumPhoto['photoUrl'] = $photoUrl;
                    $albumPhoto['thumbUrl'] = $thumbUrl;
                    
                    $gAlbums[$albumId]['photos'][] = $albumPhoto;
                    
                    
                }
            }
        } catch (App\HttpException $e) {
            echo "Error:" . $e->getMessage() . "<br />\n";
            if ($e->getResponse() != null) {
                echo "Body: <br />\n" . $e->getResponse()->getBody() . "<br />\n";
            }
        }
        
        // Возвращение объединенного массива в представление для визуализации
        return $gAlbums;
        
    }
    
    
    /*
     * Загружаем файл в google
     */
    public function processUploadGoogleAction() {
        
        //$form = $this->getServiceLocator()->get('ImageUploadGoogleForm');
        
        
        $request = $this->getRequest();
        
        if (!$request->isPost()) {
            die("Post" . $request->isPost());
            
        }
        
        $uploadFile = $this->params()->fromFiles('imageupload');    //получает данные о файле из формы поле imageupload
        
        //$form->setData($request->getPost());
        $albumId = $request->getPost()->get('album_id');
        
        
        $uploadPath = $this->getFileUploadLocation();
                
        //Сохранение выгруженного файла
        $adapter = new \Zend\File\Transfer\Adapter\Http();
        $adapter->setDestination($uploadPath);
        if (!$adapter->receive($uploadFile['name'])) {
            die('adapter');
        }
        
        $filePathName = $uploadPath . '\\' . $uploadFile['name'];
        
        $reader = new Ini();
        $dataAuthGoogle = $reader->fromFile(__DIR__ . '/../../../config/google.ini');
        
        $this->GOOGLE_USER_ID = $dataAuthGoogle['username'];
	$this->GOOGLE_PASSWORD = $dataAuthGoogle['password'];
        
        
        /*
         * отключаем sslverifypeer
         */
        $adapter = new \Zend\Http\Client\Adapter\Curl();
        $adapter->setOptions(array(
            'curloptions' => array(
                CURLOPT_SSL_VERIFYPEER => false,
            )
        ));
        
        $httpClient = new \ZendGData\HttpClient();
        $httpClient->setAdapter($adapter);
        
        $client = \ZendGData\ClientLogin::getHttpClient(
                $this->GOOGLE_USER_ID,
                $this->GOOGLE_PASSWORD,
                \ZendGData\Photos::AUTH_SERVICE_NAME,
                $httpClient);
        
        
         
        /*
         * создать нового клиента Google Photos с помощью API-клиента
         */
        $service = new \ZendGData\Photos($client);
        
        //$gAlbums = array();
        
        try {
            
            $fd = $service->newMediaFileSource($filePathName);
            //$fd = $service->newMediaFileSource('C:\xampp\htdocs\CommunicationApp\module\Users\data\images\i.jpeg');
            
            $fd->setContentType('image/jpeg');
            
            $entry = new \ZendGData\Photos\PhotoEntry();
            $entry->setMediaSource($fd);
            $entry->setTitle($service->newTitle("test photo"));
//            $photo->setCategory(array($client->newCategory(
//                'http://schemas.google.com/photos/2007#photo',
//                'http://schemas.google.com/g/2005#kind')));
            
            $albumQuery = new \ZendGData\Photos\AlbumQuery();
            $albumQuery->setUser($this->GOOGLE_USER_ID);
            //$albumQuery->setAlbumName("***");
            $albumQuery->setAlbumId($albumId);

            $albumEntry = $service->getAlbumEntry($albumQuery);

            $newPhoto = $service->insertPhotoEntry($entry, $albumEntry);
            
            var_dump($newPhoto);
            
        } catch (App\HttpException $ex) {
            echo "Error:" . $ex->getMessage() . "<br />\n";
            if ($ex->getResponse() != null) {
                echo "Body: <br />\n" . $ex->getResponse()->getBody() . "<br />\n";
            }
        }
        
        
        
        
    }
    
    
    public function addGooglePhoto() {
        
        $reader = new Ini();
        $dataAuthGoogle = $reader->fromFile(__DIR__ . '/../../../config/google.ini');
        
        $this->GOOGLE_USER_ID = $dataAuthGoogle['username'];
	$this->GOOGLE_PASSWORD = $dataAuthGoogle['password'];
        
        
        /*
         * отключаем sslverifypeer
         */
        $adapter = new \Zend\Http\Client\Adapter\Curl();
        $adapter->setOptions(array(
            'curloptions' => array(
                CURLOPT_SSL_VERIFYPEER => false,
            )
        ));
        
        $httpClient = new \ZendGData\HttpClient();
        $httpClient->setAdapter($adapter);
        
        $client = \ZendGData\ClientLogin::getHttpClient(
                $this->GOOGLE_USER_ID,
                $this->GOOGLE_PASSWORD,
                \ZendGData\Photos::AUTH_SERVICE_NAME,
                $httpClient);
        
        /*
         * создать нового клиента Google Photos с помощью API-клиента
         */
        $gp = new \ZendGData\Photos($client);
        
        
        
        $gAlbums = array();
        
        try {
            $userFeed = $gp->getUserFeed($this->GOOGLE_USER_ID);    //Получить список альбомов с помощью функции getUserFeed()
            foreach ($userFeed as $userEntry) {
                $albumId = $userEntry->getGphotoId()->getText();    //id альбома
                $gAlbums[$albumId]['label'] = $userEntry->getTitle()->getText();    //название альбома
                
                $query = $gp->newAlbumQuery();
                $query->setUser($this->GOOGLE_USER_ID);
                $query->setAlbumId($albumId);
                
                $albumFeed = $gp->getAlbumFeed($query); //Получить список изображений в альбоме с помощью функции getAlbumFeed()
                
                foreach ($albumFeed as $photoEntry) {
                    $photoId = $photoEntry->getGphotoId()->getText();
                    if ($photoEntry->getMediaGroup()->getContent() != null) {
                        $mediaContentArray = $photoEntry->getMediaGroup()->getContent();
                        $photoUrl = $mediaContentArray[0]->getUrl();
                    }
                    
                    if ($photoEntry->getMediaGroup()->getThumbnail() != null) {
                        $mediaThumbnailArray = $photoEntry->getMediaGroup()->getThumbnail();
                        $thumbUrl = $mediaThumbnailArray[0]->getUrl();
                    }
                    
                    $albumPhoto = array();
                    $albumPhoto['id'] = $photoId;
                    $albumPhoto['photoUrl'] = $photoUrl;
                    $albumPhoto['thumbUrl'] = $thumbUrl;
                    
                    $gAlbums[$albumId]['photos'][] = $albumPhoto;
                    
                    
                }
            }
        } catch (App\HttpException $e) {
            echo "Error:" . $e->getMessage() . "<br />\n";
            if ($e->getResponse() != null) {
                echo "Body: <br />\n" . $e->getResponse()->getBody() . "<br />\n";
            }
        }
        
        // Возвращение объединенного массива в представление для визуализации
        return $gAlbums;
        
    }
    
    
    public function getYoutubeVideos() {
        set_time_limit(100);
        /*
         * Установить соединение
         */
        $adapter = new \Zend\Http\Client\Adapter\Curl();
        $adapter->setOptions(array(
            'curloptions' => array(
                CURLOPT_SSL_VERIFYPEER => false,
            )
        ));
        $httpClient = new \ZendGData\HttpClient();
        $httpClient->setAdapter($adapter);
        
        $client = \ZendGData\ClientLogin::getHttpClient(
                $this->GOOGLE_USER_ID,
                $this->GOOGLE_PASSWORD,
                \ZendGData\YouTube::AUTH_SERVICE_NAME,
                $httpClient);
        
        /*
         * Инициализировать клиента YouTube и выполнить запрос видеоролика по 
         * ключевому слову Zend Framework
         */
        $yt = new \ZendGData\YouTube($client);
        $yt->setMajorProtocolVersion(2);
        $query = $yt->newVideoQuery();
        $query->setOrderBy('relevance');
        $query->setSafeSearch('none');
        $query->setVideoQuery('Zend Framework 2');
        
        /*
         * Обратите внимание, что мы должны передать номер версии запроса 
         * URL функции для обеспечения обратной совместимости с версией 1 API.
         */
        $videoFeed = $yt->getVideoFeed($query->getQueryUrl(2));
        
        $yVideos = array();
        foreach ($videoFeed as $videoEntry) {
            $yVideo = array();
            $yVideo['videoTitle'] = $videoEntry->getVideoTitle();
            $yVideo['videoDescription'] = $videoEntry->getVideoDescription();
            $yVideo['watchPage'] = $videoEntry->getVideoWatchPageUrl();
            $yVideo['duration'] = $videoEntry->getVideoDuration();
            $videoThumbnails = $videoEntry->getVideoThumbnails();
            $yVideo['thumbnailUrl'] = $videoThumbnails[0]['url'];
            $yVideos[] = $yVideo;
        }
        
        return $yVideos;
        
    }
    
        
}




















