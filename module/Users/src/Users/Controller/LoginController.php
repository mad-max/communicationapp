<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Users\Form\LoginForm;
use Users\Form\LoginFilter;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

class LoginController extends AbstractActionController {
    
    protected $authservice;
    
    public function indexAction() { 
        /*
         * Изменяем блоки существующего кода так, чтобы они использовали 
         * менеджер служб.
         * Менеджер служб обладает огромным преимуществом с точки зрения 
         * чистоты кода, обеспечивает высокоэффективное масштабирование и 
         * служит централизованным реестром для ключевых компонентов приложения.
         */
        //$form=new LoginForm();    //было
        $form = $this->getServiceLocator()->get('LoginForm'); //стало
        $viewModel=new ViewModel(array('form'=>$form));
        return $viewModel;
    }   //indexAction
    
    public function processAction() {
        
        /*
         * Проверяем массив post, если ошибка, то переходим на начальную страницу
         */
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute(NULL,
                    array('controller'=>'login',
                        'action'=>'index'
                        ));
        }
        $post=$this->request->getPost();
        
        /*
         * Изменяем блоки существующего кода так, чтобы они использовали 
         * менеджер служб.
         * Менеджер служб обладает огромным преимуществом с точки зрения 
         * чистоты кода, обеспечивает высокоэффективное масштабирование и 
         * служит централизованным реестром для ключевых компонентов приложения.
         */
        /*
         * Создаем новый экземпляр класса LoginForm
         */
        //$form=new LoginForm();  //было
        
        /*
         * Создаем новый экземпляр класса LoginFilter
         */
        //$inputFilter=new LoginFilter();  //было
        
        /*
         * Применение фильтра LoginFilter к форме.
         */
        //$form->setInputFilter($inputFilter);  //было
        $form = $this->getServiceLocator()->get('LoginForm'); //стало
        
        /*
         * Данные, введенные в форму, добавляются еще раз, и выполняется их 
         * проверка методом isValid().
         */
        $form->setData($post);
        if (!$form->isValid()) {
            $model=new ViewModel(array(
                'error'=>true,  //Проверяется в шаблоне login/index.phtml.
                'form'=>$form,  //экземпляр класса LoginForm
            ));
            $model->setTemplate('users/login/index');   //Устанавливаем шаблон
            return $model;  //Возвращаем старую страницу с описаниями ошибок (их получаем с помощью formElementErrors). 
        }
        
        /*
         * проверьте корректность данных формы и воспользуйтесь методом 
         * AuthService, чтобы проверить учетные данные методом authenticate.
         */
        $this->getAuthService()->getAdapter()->setIdentity(
                $this->request->getPost('email'))->setCredential(
                        $this->request->getPost('password'));
        $result = $this->getAuthService()->authenticate();
        if ($result->isValid()) {
            $this->getAuthService()->getStorage()->write(
                    $this->request->getPost('email'));
            return $this->redirect()->toRoute(NULL, array(
                'controller' => 'login',
                'action' => 'confirm'
            ));
        }
        
        
        return $this->redirect()->toRoute(NULL, array(  //Плагин  redirect  позволяет  пересылать  клиента  на  другой  адрес
            'controller'=>'login',
            'action'=>'index'
        ));
    }   //processAction
    
    /*
     * Функция возвращает службу аутентификации
     */
   public function getAuthService() {
       if (!$this->authservice) {
           /*
            * Изменяем блоки существующего кода так, чтобы они использовали 
            * менеджер служб.
            * Менеджер служб обладает огромным преимуществом с точки зрения 
            * чистоты кода, обеспечивает высокоэффективное масштабирование и 
            * служит централизованным реестром для ключевых компонентов приложения.
            */
           /*
            * Содержит данные для подключения к БД
            */
           //$dbAdapter=$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'); //было
           
           /*
            * DbTable предоставляет возможность аутентификации с учетными 
            * данными, хранящимися в БД.
            */
           //$dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'user', 
           //        'email', 'password', 'MD5(?)');    //было
           //print_r($dbTableAuthAdapter);
           //$authService = new AuthenticationService();  //экземпляр сервиса аутенификации
           //$authService->setAdapter($dbTableAuthAdapter); //было
           $authService = $this->getServiceLocator()->get('AuthService'); //стало
           $this->authservice = $authService;
       }
       return $this->authservice;
   }    //getAuthService

   
    public function confirmAction() {
        $this->layout('layout/myaccount');  
        $user_email = $this->getAuthService()->getStorage()->read();
        $viewModel = new ViewModel(array(
            'user_email' => $user_email
        ));
        return $viewModel;
    }
    
    
    public function logoutAction() {
        $this->getAuthService()->clearIdentity();
        
        return $this->redirect()->toRoute('users/login');
    }
    
    
}   //LoginController




















