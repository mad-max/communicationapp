<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Users\Form\RegisterForm;
use Users\Form\RegisterFilter;
use Users\Model\User;
use Users\Model\UserTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use Zend\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

use Zend\Config\Reader\Ini;


class GroupChatController extends AbstractActionController {
    
    protected $storage;
    protected $authservice;
    
    
    protected function getAuthService() {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }

        return $this->authservice;
    }

    /*
     *  Выполняет запрос к таблице chat_messages, считывает из нее все записи 
     * и передает их в представление
     */
    public function messageListAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $chatMessageTG = $this->getServiceLocator()->get(
                'ChatMessagesTableGateway');
        $chatMessages = $chatMessageTG->select();
        
        $messageList = array();
        foreach ($chatMessages as $chatMessage) {
            $fromUser = $userTable->getUser($chatMessage->user_id);
            $messageData = array();
            $messageData['user'] = $fromUser->name;
            $messageData['time'] = $chatMessage->stamp;
            $messageData['data'] = $chatMessage->message;
            $messageList[] = $messageData;
        }
        
        $viewModel = new ViewModel(array('messageList' => $messageList));
        $viewModel->setTemplate('users/group-chat/message-list');
        $viewModel->setTerminal(true);
        return $viewModel;
    }   //messageListAction
    
    /*
     * вызывается, когда пользователь посылает сообщение, и сохраняет это 
     * сообщение в базе данных
     */
    protected function sendMessage ($messageTest, $fromUserId) {
        
        $chatMessageTG = $this->getServiceLocator()->get('ChatMessagesTableGateway');
        $data = array(
            'user_id' => $fromUserId,
            'message' => $messageTest,
            'stamp' => NULL
        );
        $chatMessageTG->insert($data);
        
        return TRUE;
    }   //sendMessage
    
    
    public function indexAction() {
        
        $user = $this->getLoggedInUser();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $messageTest = $request->getPost()->get('message');
            $fromUserId = $user->id;
            $this->sendMessage($messageTest, $fromUserId);
            // Для предотвращения дублирования записей при обновлении
            return $this->redirect()->toRoute('users/group-chat');
        }
        
        // Подготовка формы отправки сообщения
        $form = new \Zend\Form\Form();
        
        $form->add(array(
            'name' => 'message',
            'attributes' => array(
                'type' => 'text',
                'id' => 'messageText',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Message',
            ),
        ));
        
        $form->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Send'
            ),
        ));
        
        $form->add(array(
            'name' =>'refresh',
            'attributes' => array(
                'type' => 'button',
                'id' => 'btnRefresh',
                'value' => 'Refresh'
            ),
        ));
        
        $viewModel = new ViewModel(array('form' => $form, 'userName' => $user->name));
        return $viewModel;
    }
    
    
    protected function getLoggedInUser() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userEmail = $this->getAuthService()->getStorage()->read();
        $user = $userTable->getUserByEmail($userEmail);

        return $user;
    }
    
    
    public function sendOfflineMessageAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $allUsers = $userTable->fetchAll();
        $usersList = array();
        foreach($allUsers as $user) {
                $usersList[$user->id] = $user->name . '(' . $user->email . ')';
        }

        $user = $this->getLoggedInUser();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $msgSubj = $request->getPost()->get('messageSubject');
            $msgText = $request->getPost()->get('message');
            $toUser = $request->getPost()->get('toUserId');
            $fromUser = $user->id;
            $this->sendOfflineMessage($msgSubj, $msgText, $fromUser, $toUser);
            // to prevent duplicate entries on refresh
            return $this->redirect()->toRoute('users/group-chat', 
                                     array('action' => 'sendOfflineMessage'));
        }

        //Prepare Send Message Form
        $form    = new \Zend\Form\Form();
        $form->setAttribute('method', 'post');
        $form->setAttribute('enctype','multipart/form-data');

        $form->add(array(
                'name' => 'toUserId',
                'type'  => 'Zend\Form\Element\Select',
                'attributes' => array(
                        'type'  => 'select',
                ),
                'options' => array(
                        'label' => 'To User',
                ),
        ));

        $form->add(array(
                'name' => 'messageSubject',
                'attributes' => array(
                        'type'  => 'text',
                        'id' => 'messageSubject',
                        'required' => 'required'
                ),
                'options' => array(
                        'label' => 'Subject',
                ),
        ));

        $form->add(array(
                'name' => 'message',
                'attributes' => array(
                        'type'  => 'textarea',
                        'id' => 'message',
                        'required' => 'required'
                ),
                'options' => array(
                        'label' => 'Message',
                ),
        ));

        $form->add(array(
                'name' => 'submit',
                'attributes' => array(
                        'type'  => 'submit',
                        'value' => 'Send'
                ),
                'options' => array(
                        'label' => 'Send',
                ),			
        ));

        $form->get('toUserId')->setValueOptions($usersList);
        $viewModel  = new ViewModel(array('form' => $form, 
                                                                'userName' => $user->name));
        return $viewModel;
    }

    protected function sendOfflineMessage($msgSubj, $msgText, $fromUserId, $toUserId) {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $fromUser = $userTable->getUser($fromUserId);
        $toUser = $userTable->getUser($toUserId);

        $mail = new Mail\Message();
        $mail->setFrom($fromUser->email, $fromUser->name);
        $mail->addTo($toUser->email, $toUser->name);
        $mail->setSubject($msgSubj);
        $mail->setBody($msgText);
        
        $headers = $mail->getHeaders();
        $headers->removeHeader('Content-Type');
        $headers->addHeaderLine('Content-Type', 'text/plain; charset=UTF-8');
        
        $reader = new Ini();
        $data   = $reader->fromFile(__DIR__ . '/../../../config/smtp.ini');

        // Setup SMTP transport using PLAIN authentication over TLS
        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'name'              => $data['name'],
            'host'              => $data['host'],
            'port'              => 587, // Notice port change for TLS is 587
            'connection_class'  => $data['connection_class'],
            'connection_config' => array(
                'username' => $data['username'],
                'password' => $data['password'],
                'ssl'      => 'tls',
            ),
        ));
        
        $transport->setOptions($options);

        $transport->send($mail);

        return true;
        
    }	//sendOfflineMessage
    
    
    
    
    
    
    
    
    
    
    
}









