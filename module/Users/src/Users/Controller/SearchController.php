<?php
namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Http\Headers;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use Users\Form\RegisterForm;
use Users\Form\RegisterFilter;

use Users\Model\User;
use Users\Model\UserTable;
use Users\Model\Upload;

use Users\Model\ImageUpload;
use Users\Model\ImageUploadTable;

use ZendSearch\Lucene;
use ZendSearch\Lucene\Document;
use ZendSearch\Lucene\Index;

//'C:\xampp\htdocs\CommunicationApp\vendor\ZendSearch\library\ZendSearch\Lucene\\';

class SearchController extends AbstractActionController {
    
    protected $storage;
    protected $authservice;
    
    
    
    /*
     * Метод для выборки местоположения индекса из конфигурационных
     * данных модуля
     */
    public function getIndexLocation() {
        $config = $this->getServiceLocator()->get('config');
        //Traversable - Интерфейс, определяющий, является ли класс обходимым (traversable) используя foreach. 
        if ($config instanceof \Traversable) {  //Оператор instanceof используется для определения того, является ли текущий объект экземпляром указанного класса.
            $config = ArrayUtils::iteratorToArray($config); //iterator_to_array — Копирует итератор в массив
        }
        if (!empty($config['module_config']['search_index'])) {
            return $config['module_config']['search_index'];
        } else {
            return FALSE;
        }
    }
    
    public function generateIndexAction() {
        
        /*
         * Для работы с русским языком (иначе выдает ошибку illegal character)
         */
        Lucene\Analysis\Analyzer\Analyzer::setDefault(
            new Lucene\Analysis\Analyzer\Common\Utf8\CaseInsensitive()
        );
        
        $searchIndexLocation = $this->getIndexLocation();
        $index = Lucene\Lucene::create($searchIndexLocation);
                
        $userTable = $this->getServiceLocator()->get('UserTable');
        $uploadTable = $this->getServiceLocator()->get('UploadTable');
        $allUploads = $uploadTable->fetchAll();
        foreach($allUploads as $fileUpload) {
            $uploadOwner = $userTable->getUser($fileUpload->user_id);
            
            //создание полей lucene
            $fileUploadId = Document\Field::unIndexed('upload_id', $fileUpload->id);    //Неиндексированное поле, которое будет использоваться для получения выгруженного файла, возвращаемого в результатах поиска
            $label = Document\Field::Text('label', $fileUpload->label); //Это поле используется для индексирования поля label таблицы uploads
            $owner = Document\Field::Text('owner', $uploadOwner->name); //Это поле служит для индексирования поля name пользователя, который выгрузил документ

            if (substr_compare($fileUpload->filename, ".xlsx", //Бинарно-безопасное сравнение 2 строк со смещением, с учетом или без учета регистра 
                strlen($fileUpload->filename)-strlen(".xlsx"), //сравниваем расширение файла с .xlsx
                strlen(".xlsx")) ===0) {
                
                //индексирование таблицы excel
                $uploadPath = $this->getFileUploadLocation();
                $indexDoc = Lucene\Document\Xlsx::loadXlsxFile($uploadPath 
                        . "/" . $fileUpload->filename);
        
            } else if (substr_compare($fileUpload->filename, ".docx", //Бинарно-безопасное сравнение 2 строк со смещением, с учетом или без учета регистра 
                strlen($fileUpload->filename)-strlen(".docx"), //сравниваем расширение файла с .xlsx
                strlen(".docx")) ===0) {
                
                //индексируем файл word
                $uploadPath = $this->getFileUploadLocation();
                $indexDoc = Lucene\Document\Docx::loadDocxFile($uploadPath
                        . "/" . $fileUpload->filename);
                
            } else {
                //создание нового документа
                $indexDoc = new Lucene\Document();
            }
                
            //добавление всех полей
            $indexDoc->addField($label);
            $indexDoc->addField($owner);
            $indexDoc->addField($fileUploadId);
            $index->addDocument($indexDoc);
        }
        $index->commit();
    }
    
    public function indexAction() {
        
        $request = $this->getRequest();
        $searchResults=null;    //без этого вылазит ошибка Undefined variable
        if($request->isPost()) {
            $queryText = $request->getPost()->get('query');
            $searchIndexLocation = $this->getIndexLocation();
            Lucene\Analysis\Analyzer\Analyzer::setDefault(
                new Lucene\Analysis\Analyzer\Common\Utf8\CaseInsensitive()  //Для работы с русским языком (иначе выдает ошибку illegal character)
            );
            $index = Lucene\Lucene::open($searchIndexLocation);
            $searchResults = $index->find($queryText);
        }
        
        //Подготовка формы поиска
        $form = new \Zend\Form\Form();
        $form->add(array(
            'name' => 'query',
            'attributes' => array(
                'type' => 'text',
                'id' => 'queryText',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Search String',
            ),
        ));
        
        $form->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search'
            ),
        ));
        
        $viewModel = new ViewModel(array(
            'form' => $form,
            'searchResults' => $searchResults
        ));
        
        return $viewModel;
    }
    
    
    public function getFileUploadLocation() {
        $config = $this->getServiceLocator()->get('config');
        if ($config instanceof \Traversable) {
            $config = ArrayUtils::iteratorToArray($config);
        }
        if(!empty($config['module_config']['upload_location'])) {
            return $config['module_config']['upload_location'];
        } else {
            return FALSE;
        }
    }
    
    
}












