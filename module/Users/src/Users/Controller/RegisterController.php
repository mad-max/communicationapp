<?php

namespace Users\Controller;


use Zend\Mvc\Controller\AbstractActionController;   //создание псевдонима имени
use Zend\View\Model\ViewModel;
use Users\Form\RegisterForm;
use Users\Form\RegisterFilter;
use Users\Model\User;
use Users\Model\UserTable;

class RegisterController extends AbstractActionController {
    
    public function indexAction() {
        /*
         * Изменяем блоки существующего кода так, чтобы они использовали 
         * менеджер служб.
         * Менеджер служб обладает огромным преимуществом с точки зрения 
         * чистоты кода, обеспечивает высокоэффективное масштабирование и 
         * служит централизованным реестром для ключевых компонентов приложения.
         */
        //$form=new RegisterForm();   //было
        $form = $this->getServiceLocator()->get('RegisterForm'); //стало
        $viewModel=new ViewModel(array('form'=>$form));
        return $viewModel;
    }   //indexAction
    
    public function confirmAction() {
        $viewModel=new ViewModel(); //Почему нет $view->setTemplate('users/index/confirm'); ?
        return $viewModel;
    }   //confirmAction
    
    public function processAction() {
        
        /*
         * Проверяем массив post, если ошибка, то переходим на начальную страницу
         */
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute(NULL,
                    array('controller'=>'register',
                        'action'=>'index'
                        ));
        }
        $post=$this->request->getPost();
        
        /*
        * Изменяем блоки существующего кода так, чтобы они использовали 
        * менеджер служб.
        * Менеджер служб обладает огромным преимуществом с точки зрения 
        * чистоты кода, обеспечивает высокоэффективное масштабирование и 
        * служит централизованным реестром для ключевых компонентов приложения.
        */
        /*
         * Создаем новый экземпляр класса RegisterForm
         */
        //$form=new RegisterForm();
        
        /*
         * Создаем новый экземпляр класса RegisterFilter
         */
        //$inputFilter=new RegisterFilter();
        
        /*
         * Применение фильтра RegisterFilter к форме.
         */
        //$form->setInputFilter($inputFilter);
        $form = $this->getServiceLocator()->get('RegisterForm'); //стало
        
        /*
         * Данные, введенные в форму, добавляются еще раз, и выполняется их 
         * проверка методом isValid().
         */
        $form->setData($post);
        if (!$form->isValid()) {
            $model=new ViewModel(array(
                'error'=>true,  //Проверяется в шаблоне register/index.phtml.
                'form'=>$form, //экземпляр класса RegisterForm
            ));
            $model->setTemplate('users/register/index'); //Устанавливаем шаблон
            return $model;  //Возвращаем старую страницу с описаниями ошибок (их получаем с помощью formElementErrors). 
        }
        
        /*
         * Создание пользователя
         */
        $this->createUser($form->getData());
        
        /*
         * Перенаправления на страницу (подтверждения<-было) со списком пользователей
         */
        return $this->redirect()->toRoute('users/user-manager', array(
            'action'=>'index'
        ));
    }   //processAction
    
    
    /*
     * Сохраняет User в бд
     */
    protected function createUser(array $data) {
       /*
        * Изменяем блоки существующего кода так, чтобы они использовали 
        * менеджер служб.
        * Менеджер служб обладает огромным преимуществом с точки зрения 
        * чистоты кода, обеспечивает высокоэффективное масштабирование и 
        * служит централизованным реестром для ключевых компонентов приложения.
        */
        //$sm=$this->getServiceLocator();   //было
        
        /*
         * Содержит данные для подключения к БД
         */
        //$dbAdapter=$sm->get('Zend\Db\Adapter\Adapter');   //было
        
        /*
         * ResultSet определят из свойств объекта какие поля существуют в 
         * таблице БД для запросов к ней.
         */
        //$resultSetPrototype=new \Zend\Db\ResultSet\ResultSet();   //было
        
        /*
         * Содержит Users\Model\User Object ( [id] => [name] => [email] => 
         * [password] => ) 
         */
        //$resultSetPrototype->setArrayObjectPrototype(new \Users\Model\User);   //было
        
        /*
         * TableGateway реализует наиболее часто используемые операции работы с БД.
         * Содержит в себе SQL запросы к БД. 
         */
        //$tableGateway=new \Zend\Db\TableGateway\TableGateway('user', $dbAdapter, 
        //        null, $resultSetPrototype);   //было
        //$tableGateway = $this->getServiceLocator()->get('UserTableGateway'); //стало
        
        /*
         * Наш класс User содержит данные пользователя (имя, пароль и т.д.)
         */
        $user=new User();
        $user->exchangeArray($data);
        
       /*
        * Изменяем блоки существующего кода так, чтобы они использовали 
        * менеджер служб.
        * Менеджер служб обладает огромным преимуществом с точки зрения 
        * чистоты кода, обеспечивает высокоэффективное масштабирование и 
        * служит централизованным реестром для ключевых компонентов приложения.
        */
        /*
         * Наш класс userTable для работы с БД
         */
        //$userTable=new UserTable($tableGateway);   //было
        $userTable = $this->getServiceLocator()->get('UserTable'); //стало
        $userTable->saveUser($user);
        
        return TRUE;
        
    }   //createUser
    
    
}   //RegisterController



















