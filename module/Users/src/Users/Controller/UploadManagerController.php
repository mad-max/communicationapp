<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Http\Headers;

use Users\Model\Upload;
//use Users\Model\UploadTable;

use Users\Model\User;
use Users\Model\UserTable;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use Users\Form\RegisterForm;
use Users\Form\RegisterFilter;

class UploadManagerController extends AbstractActionController {
    
    protected $authservice;
    protected $storage;
    
    /*
     * Возвращает страницу со списком выгруженных файлов юзера и списком 
     * доступных ему файлов других пользователей
     */
    public function indexAction() {
        
        $uploadTable = $this->getServiceLocator()->get('UploadTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        //Получение информации о пользователе от сеанса
        $userEmail = $this->getAuthService()->getStorage()->read(); //в LoginController в хранилище записывается только email
        $user = $userTable->getUserByEmail($userEmail);
        $sharedUploads = $uploadTable->getSharedUploadsForUserId($user->id);
        $sharedUploadsList = null;
        foreach($sharedUploads as $sharedUpload) {
            $uploadOwner = $userTable->getUser($sharedUpload->user_id);
            $sharedUploadInfo = array();
            $sharedUploadInfo['label'] = $sharedUpload->label;
            $sharedUploadInfo['owner'] = $uploadOwner->name;
            $sharedUploadsList[$sharedUpload->id] = $sharedUploadInfo;
        }

        $viewModel  = new ViewModel(
            array(
                'myUploads' => $uploadTable->getUploadsByUserId($user->id),
                'sharedUploadsList' => $sharedUploadsList    				
                )
        );
        return $viewModel;
    }
    
    /*
     * Возвращает страницу для загрузки на сервер файла 
     */
    public function uploadAction() {
        $form = $this->getServiceLocator()->get('UploadForm');
        $viewModel  = new ViewModel(array('form' => $form)); 
        return $viewModel; 
    }

    /*
     * Функция возвращает службу аутентификации
     */
    public function getAuthService() {
        if (!$this->authservice) {
           $authService = $this->getServiceLocator()->get('AuthService'); 
           $this->authservice = $authService;
        }
        return $this->authservice;
    }    //getAuthService
   
    /*
     * Загружаем файл на сервер
     */
    public function processUploadAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');   	
        $user_email = $this->getAuthService()->getStorage()->read();    	 
        $user = $userTable->getUserByEmail($user_email);
        $form = $this->getServiceLocator()->get('UploadForm');
        $request = $this->getRequest();

        if ($request->isPost()) {
            $upload = new Upload();
            $uploadFile = $this->params()->fromFiles('fileupload');
            
            $form->setData($request->getPost());
            if ($form->isValid()) {
                //Получение конфигупации из конфигурационных данных модуля
                $uploadPath = $this->getFileUploadLocation();
                
                //Сохранение выгруженного файла
                $adapter = new \Zend\File\Transfer\Adapter\Http();
                $adapter->setDestination($uploadPath);
                if ($adapter->receive($uploadFile['name'])) {
                   
                    //Успешная выгрузка файла
                    $exchange_data = array();
                    $exchange_data['label'] = $request->getPost()->get('label');
                    $exchange_data['filename'] = $uploadFile['name'];
                    $exchange_data['user_id'] = $user->id;

                    $upload->exchangeArray($exchange_data);
                    $uploadTable = $this->getServiceLocator()->get('UploadTable');
                    $uploadTable->saveUpload($upload);
                    
//                    return $this->redirect()->toRoute('users/upload-manager', 
//                            array('action' => 'index'));
                    //обновляем индекс
                    return $this->redirect()->toRoute('users/search', 
                        array('action' => 'generateIndex'));
                }
            }
        }
        return array('form' => $form); //неправильно, т.к. потом идет перенаправление на наблон process-upload.phtml а его нет
        
    }   //processUploadAction
    
    /*
     * Получение конфигурации из конфигурационных данных модуля
     */
    public function getFileUploadLocation() {
        $config  = $this->getServiceLocator()->get('config');
        return $config['module_config']['upload_location'];
    }
    
    /*
     * Удалить загруженный файл и запись о нем 
     */
    public function deleteAction() {
        
        $uploadId = $this->params()->fromRoute('id');   //получить id файла из get запроса
        $uploadTable = $this->getServiceLocator()->get('UploadTable');  //ссылка на таблицу uploads
        $upload = $uploadTable->getUpload($uploadId);   //получить из бд данные о файле
        $uploadPath = $this->getFileUploadLocation();
        unlink($uploadPath . "/" . $upload->filename);  //удалить файл
        $uploadTable->deleteUpload($uploadId);  //удалить запись в бд
        
//        return $this->redirect()->toRoute('users/upload-manager');
        //обновляем индекс
        return $this->redirect()->toRoute('users/search', 
                        array('action' => 'generateIndex'));
    }   //deleteAction
    
    /*
     * Возвращает страницу редактирования описания файла и добавления доступа к 
     * файлу и список пользователей которые имеют доступ к файлу
     */
    public function editAction() {
        $uploadId = $this->params()->fromRoute('id');   //получить id файла
        $uploadTable = $this->getServiceLocator()->get('UploadTable');  //получить объект UploadTable
        $userTable = $this->getServiceLocator()->get('UserTable');  //получить объект UserTable
        
        //UploadEditForm
        $upload = $uploadTable->getUpload($uploadId);   //информация о загруженном на сервер файле из бд
        $form = $this->getServiceLocator()->get('UploadEditForm');  //получить форму UploadEditForm
        $form->bind($upload);   //связать форму с $upload
        
        //Shared Users List
        $sharedUsers = array(); 
        $sharedUsersResult = $uploadTable->getSharedUsers($uploadId);   //список пользователей, которые имеют доступ к выгруженному файлу
        foreach ($sharedUsersResult as $sharedUserRow) {
            $user = $userTable->getUser($sharedUserRow->user_id); //получить объект User из бд согласно user_id из бд upload
            $sharedUsers[$sharedUserRow->id] = $user->name; //составляем массив из имен юзеров, которые имеют доступ к выгруженному файлу
        }
        
        //Add Additional Sharing
        $uploadShareForm = $this->getServiceLocator()->get('UploadShareForm');  //получить форму UploadShareForm
        $allUsers = $userTable->fetchAll(); //получить список всех зарегистрированных юзеров
        $userList = array();
        foreach($allUsers as $user) {
            $userList[$user->id] = $user->name; //составить массив из имен зарегистрированных юзеров для списка(select)
        }
        
        $uploadShareForm->get('upload_id')->setValue($uploadId);    //установить значение скрытого поля upload_id (id файла)
        $uploadShareForm->get('user_id')->setValueOptions($userList);   //установить значение списка (имена всех юзеров)
        
        $viewModel = new ViewModel(array('form' => $form,   //создать модель с двумя формами!
            'upload_id' => $uploadId,
            'sharedUsers' => $sharedUsers,
            'uploadShareForm' => $uploadShareForm,
            )
        );
        return $viewModel;
    }
    
    /*
     * Добавить общий доступ к файлу
     */
    public function processUploadShareAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $uploadTable = $this->getServiceLocator()->get('UploadTable');
        $form = $this->getServiceLocator()->get('UploadForm');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $userId = $request->getPost()->get('user_id');  //id юзера которому мы хотим добавить доступ
            $uploadId = $request->getPost()->get('upload_id');  //id файлу к которому хотим добавить доступ для вышеупомянутого юзера
            $uploadTable->addSharing($uploadId, $userId);   //делаем запись в бд
            return $this->redirect()->toRoute('users/upload-manager', array(    //возвращаемся на страницу редактирования загрузки
                'action' => 'edit', 'id' => $uploadId
                    )
                );
        }
    }   //processUploadShareAction
    
    /*
     * Обновляем описание к уже загруженному файлу.
     * 
     * user_id и имя файла могут быть подменены юзером. Удалить из формы user_id
     * и название файла? Можно и  upload_id подменить - получается надо 
     * проверять что данные не подменены.
     */
    public function processEditAction() {
        if(!$this->request->isPost()) {
            return $this->redirect()->toRoute('users/upload-manager', 
                    array('action' => 'edit'
            ));
        }
        
        $post = $this->request->getPost();  //получить данные из post в виде объекта
        $uploadTable = $this->getServiceLocator()->get('UploadTable');  //Получаем объект UploadTable для работы с бд
        $upload = $uploadTable->getUpload($post->id);   //Получаем объект Upload из бд согласно id
        
        $form = $this->getServiceLocator()->get('UploadEditForm');  //получить объект Form (пустой, данных в нем нет)
        $form->bind($upload);   //связать объекты Form и Upload (данные из Upload копируются в Form)
        $form->setData($post);  //установить в форму данные из post
        if (! $form->isValid()) {   //проверяем форму
            $model = new ViewModel(array(   //если данные от юзера невалидны то устанавливаем флаг error и возвращаем юзера обратно с описанием ошибочных данных
                'error' => true,
                'form' => $form,
            ));
            $model->setTemplate('users/upload-manager/edit');
            return $model;
        }
        $this->getServiceLocator()->get('UploadTable')->saveUpload($upload);    //сохраняем Upload в бд (данные в $upload копируются из формы автоматически если она прошла проверку,т.к. эти объкты связаны
        return $this->redirect()->toRoute('users/upload-manager');  //redirect
    }   //processEditAction
    
    /*
     * Скачивание общедоступные файлы
     */
    public function fileDownloadAction() {
        $uploadId = $this->params()->fromRoute('id'); //id требуемого файла
        $uploadTable = $this->getServiceLocator()->get('UploadTable');
        $upload = $uploadTable->getUpload($uploadId);   //получить информацию о файле из бд (объект)
        
        // Считывание конфигурации из модуля
        $uploadPath = $this->getFileUploadLocation();   //получить путь к папке с загруженными файлами
        $file = file_get_contents($uploadPath . "/" . $upload->filename); //путь к файлу
        
        // Непосредственное возвращение ответа
        $response = $this->getEvent()->getResponse();
        $response->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment;filename="' .$upload->filename . '"',
        ));
        $response->setContent($file);
        return $response;
    }
    
    
}   //UploadManagerController
















