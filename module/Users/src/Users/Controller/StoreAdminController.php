<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class StoreAdminController extends AbstractActionController {
    
    /*
     * перечисление всех продуктов
     */
    public function indexAction() {
        
        $productsTable = $this->getServiceLocator()->get('StoreProductsTable');
        $storeProducts = $productsTable->fetchAll();
        
        $viewModel = new ViewModel(
            array(
                'storeProducts' => $storeProducts
            )
        );
        
        return $viewModel;
        
    }
    
    
    /*
     * перечисление всех заказов
     */
    public function listOrdersAction() {
        $storeOrdersTable = $this->getServiceLocator()->get('StoreOrdersTable');
        $storeOrders = $storeOrdersTable->fetchAll();
        
        $viewModel = new ViewModel(array(
            'storeOrders' => $storeOrders
        ));
        
        return $viewModel;
        
    }
    
    
    /*
     * просмотр конкретного заказа
     */
    public function viewOrderAction() {
        
        $orderId = $this->params()->fromRoute('id');
        $storeOrdersTable = $this->getServiceLocator()->get('StoreOrdersTable');
        $storeOrder = $storeOrdersTable->getOrder($orderId);
        
        $viewModel = new ViewModel(array(
            'storeOrder' => $storeOrder,
            'orderProduct' => $storeOrder->getProduct(),
        ));
        
        return $viewModel;
    }
    
    
    /*
     * удаление существующего продукта
     */
    public function deleteProductAction() {
        
        $productId = $this->params()->fromRoute('id');
        $storeProductsTG = $this->getServiceLocator()->get('StoreProductsTableGateway');
        $storeProductsTG->delete(array('id' => $productId));
        
        return $this->redirect()->toRoute('users/store-admin');
    }
    
    
    /*
     * обновление статуса заказа
     */
    public function updateOrderStatusAction() {
        
        $orderId = $this->params()->fromRoute('id');
        $newOrderStatus = $this->params()->fromRoute('subaction');
        
        $storeOrderTG = $this->getServiceLocator()->get('StoreOrdersTableGateway');
        $storeOrderTG->update(array('status' => $newOrderStatus), array('id' => $orderId));
        
        return $this->redirect()->toRoute('users/store-admin', array('action' => 'viewOrder', 'id' => $orderId));
        
    }
    
}
