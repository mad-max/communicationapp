<?php

namespace Users\Form;

use Zend\InputFilter\InputFilter;

class RegisterFilter extends InputFilter
{
    public function __construct() 
    {
        /*
         * для поля Email Addressмы добавим валидатор, который проверяет, 
         * является ли введенное значение корректным адресом электронной почты
         */
        $this->add(array(
            'name'=>'email',
            'required'=>true,
            'validators'=>array(
                array(
                    'name'=>'EmailAddress',
                    'options'=>array(
                        'domain'=>true,
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'name',
            'required'=>true,
            'filters'=>array(
                array(
                    'name'=>'StripTags',
                ),
            ),
            'validators'=>array(
                array(
                    'name'=>'StringLength',
                    'options'=>array(
                        'encoding'=>'UTF-8',
                        'min'=>2,
                        'max'=>148,
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'password',
            'required'=>true,
        ));

        $this->add(array(
            'name'=>'confirm_password',
            'required'=>true,
        ));

    }   //__construct
    
}   //RegisterFilter