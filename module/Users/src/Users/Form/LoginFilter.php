<?php

namespace Users\Form;

use Zend\InputFilter\InputFilter;

class LoginFilter extends InputFilter
{
    public function __construct() 
    {
        /*
         * для поля Email Addressмы добавим валидатор, который проверяет, 
         * является ли введенное значение корректным адресом электронной почты
         */
        $this->add(array(
            'name'=>'email',
            'required'=>true,
            'validators'=>array(
                array(
                    'name'=>'EmailAddress',
                    'options'=>array(
                        'domain'=>true,
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name'=>'password',
            'required'=>true,
        ));
    
    }   //__construct
    
    
    
    
}   //LoginFilter