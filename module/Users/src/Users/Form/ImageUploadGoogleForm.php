<?php

namespace Users\Form;

use Zend\Form\Form;

class ImageUploadGoogleForm extends Form {
    
    public function __construct($name = null) {
        parent::__construct('Upload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        
        
        
        $this->add(array(
            'name' => 'imageupload',
            'attributes' => array(
                'type' => 'file',
            ),
            'options' => array(
                'label' => 'Image Upload',
            ),
        ));
        
        $this->add(array(
            'name'=>'album_id',
            'type' => 'Zend\Form\Element\Select',
            'attributes'=>array(
                'type'=>'select',
            ),
            'options'=>array(
                'label'=>'Album',
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Upload Now'
            ),
        ));
        
    }   //__construct
    
    
}
